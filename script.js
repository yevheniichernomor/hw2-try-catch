/* Відповіді:
Конструкцію try...catch можна використовувати для перевірки правильності вводу користувачем будь-яких
даних(імені, номера телефону, адреси, адреси електронної пошти, дати народження) або перевірки
будь-яких даних з різних джерел, можна попередити введення пустої строки, введення даних латинецею замість 
кирилиці чи навпаки, для перевірки наявності чи відсутності властивостей об'єкта або об'єктів у масиві тощо.
*/

const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

let div;
let ul;
let i;
div = document.createElement('div');
div.setAttribute('id', 'root');
document.body.appendChild(div);
ul = document.createElement('ul');
document.getElementById('root').appendChild(ul);


for (let element of books) {
  try {
    if (!element.author){
      throw new SyntaxError ('No author')
    }else if(!element.name){     
      throw new SyntaxError ('No name')
    } else if (!element.price) {
      throw new SyntaxError ('No price')
    } else {
      let listItem = document.createElement('li');
      listItem.innerHTML = JSON.stringify(element, null, '  ')
      ul.append(listItem);
    }    
  }
  
  catch(err) {
      console.log(err)
  }
}